package com.lagou;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author liuyj
 * @Title: SimpleBean
 * @create 2021-07-06 10:22
 * @ProjectName zdy-spring-boot-starter
 * @Description: TODO
 */
@EnableConfigurationProperties(SimpleBean.class)
@ConfigurationProperties(prefix = "simplebean")
public class SimpleBean {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public SimpleBean setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public SimpleBean setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "SimpleBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
