package com.lagou;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuyj
 * @Title: MyAutoConfiguration
 * @create 2021-07-06 10:26
 * @ProjectName zdy-spring-boot-starter
 * @Description: TODO
 */
@Configuration
@ConditionalOnClass //当类路径classpath下有指定的类的情况下进行自动配置
public class MyAutoConfiguration {

    static {
        System.out.println("MyAutoConfiguration init ...");
    }
    @Bean
    public SimpleBean simpleBean(){
        return new SimpleBean();
    }
}
